open Ppxlib

let expand ~ctxt (payload : string) =
  let loc = Expansion_context.Extension.extension_point_loc ctxt in
  Ast_builder.Default.estring ~loc (payload ^ "!")

let ext_name = "suprise"

let ext =
  Extension.V3.declare ext_name Extension.Context.expression
    Ast_pattern.(single_expr_payload (estring __))
    expand

let extender_rule = Context_free.Rule.extension ext
let _ = Driver.register_transformation ~rules:[ extender_rule ] "test_ruless"
